/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/gulp-function',
  version: '3.0.5',
  description: 'accepts a function call as parameter to execute in gulp pipeline'
}
